const JonSnow = require('./variable');

const ejemploJSON = JonSnow.jsonGOT;

var variableClonada = JSON.parse(JSON.stringify(ejemploJSON));

const nombre = variableClonada.name;
console.log('El nombre del personaje es:', nombre);

const aliasFavorito = variableClonada.aliases[1];
console.log('El alias que más me gusta del personaje es:', aliasFavorito);
