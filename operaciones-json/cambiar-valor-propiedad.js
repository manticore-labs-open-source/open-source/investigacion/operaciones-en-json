const JonSnow = require('./variable');

const ejemploJSON = JonSnow.jsonGOT;

var variableClonada = JSON.parse(JSON.stringify(ejemploJSON));

console.log('Nombre original:', variableClonada.name);

variableClonada.name ='Aegon Targaryen';

console.log('El nuevo nombre es: ', variableClonada.name);