const JonSnow = require('./variable');

const ejemploJSON = JonSnow.jsonGOT;

var variableClonada = JSON.parse(JSON.stringify(ejemploJSON));

console.log('JSON antes de eliminar la variable: ', variableClonada);

delete variableClonada.born;

console.log('JSON después de eliminar la variable escogida:', variableClonada);