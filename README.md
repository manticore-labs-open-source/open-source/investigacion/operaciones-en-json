# Operaciones en JSON

## ¿Qué es un JSON?

De manera sencilla, se puede definir a un JSON como un objeto, el mismo que es normalmente utilizado para enviar datos a través de aplicaciones web.

## Operaciones comunes sobre objetos JSON

Para poder explicar las operaciones básicas sobre estos objetos, es necesario contar con un JSON, para este caso, se ha seleccionado un JSON de la API de Game of Thrones, la cual, puede encontrarse [aquí](https://anapioficeandfire.com/).
El archivo que contiene el JSON utilizado puede ser visualizado [aquí](https://gitlab.com/manticore-labs/open-source/investigacion/operaciones-en-json/blob/master/operaciones-json/variable.js).
Ahora bien, con el objeto escogido, se procede a importarlo en los scripts utilizados, es recomendable realizar un clon del objeto para que, al momento de modificar sus atributos, no afecte al objeto original. 
Para realizar el clon del objeto dentro de un archivo importado, se debe realizar lo siguiente:

```
const JonSnow = require('./variable');

const ejemploJSON = JonSnow.jsonGOT;

var variableClonada = JSON.parse(JSON.stringify(ejemploJSON));
```

Con esto, se asegura que el JSON original no será afectado con ciertas operaciones.

### Acceso a los atributos

Dentro de un archivo JSON pueden existir distintos tipos de atributos, pueden ser un atributo representado por un string o un arreglo de otros atributos.
Para acceder al primer tipo de atributos, basta con indicar el atributo deseado de la siguiente manera:

```
const nombre = variableClonada.name;
console.log('El nombre del personaje es:', nombre);
```

Para acceder a los atributos que se encuentren dentro de un arreglo, se debe realizar lo siguiente: 

```
const aliasFavorito = variableClonada.aliases[1];
console.log('El alias que más me gusta del personaje es:', aliasFavorito);
```

Si se está accediendo de manera correcta a los distintos atributos, se podrá observar, al momento de ejecución, algo como lo siguiente:

![Acceso a los atributos](./imagenes/acceso-atributos.png)

El acceso a los atributos debe de ser la operación más utilizada, pues a través de esta, se puede manipular y filtrar los datos según convenga.

### Aumentar una nueva propiedad a un atributo

Los atributos representados por arreglos de otros atributos tienen la particular ventaja de que pueden ser incrementales de manera sencilla. 
Al tratarse de arreglos, pueden utilizarse métodos nativos como **push** para agregar nuevos atributos, así:

```
const nuevoAlias = 'Jon know nothing Snow';
variableClonada.aliases.push(nuevoAlias);
```

Como se puede observar, dentro del arreglo de alias, se está incluyendo una nueva propiedad, la cual, podrá ser visualizada en consola al ejecutar el sript, mostrando algo similar a esto:

![Aumento propiedad](./imagenes/aumentar-propiedad.png)

### Eliminar un atributo

Anteriormente, se mencionó acerca de la importancia de acceder a los atributos de un JSON para modificarlos. 
Para el caso en el que se desee eliminar un atributo, basta utilizar el comando **delete** seguido del atributo deseado, así:

```
delete variableClonada.born;
```

En este caso, se eliminará el atributo born, y a continuación se muestra el JSON con todos los atributos:

![Antes de borrar](./imagenes/antes-borrar.png)

Una vez ejecutado el script, el objeto se verá modificado:

![Después de borrar](./imagenes/despues-borrar.png)

Cabe recalcar que el objeto original sigue intacto, pues como se mencionó al inicio, se está utilizando un clon de dicho objeto.

### Cambiar el valor de un atributo

La última operación que se expondrá en este apartado es la de cambio de una propiedad, similar a la operación anterior, basta señalar la propiedad que se desea cambiar y especificar su nuevo valor así:

```
variableClonada.name ='Aegon Targaryen';
```

A continuación se muestra el resultado en consola del antes y el después de realizar esta operación:

![Cambiar valor del atributo](./imagenes/cambio-valor.png)

<a href="https://twitter.com/following" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @BAndresTorres </a><br>
<a href="https://www.facebook.com/bryan.a.torres.5" target="_blank"><img alt="Sígueme en Facebook" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Facebook"/> Andrés Torres Albuja </a><br>
<a href="https://www.instagram.com/adler.luft/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> adler.luft </a><br>
